import style from "./index.module.scss";
import { useEffect, useState } from "react";
import Taro, { useReachBottom } from "@tarojs/taro";
import { getPlayList } from "@/api";
// import CustomImage from "@/components/CustomImage";
import { toWan } from "@/utils/index";
import { Loading, Divider, Space, Empty } from "@nutui/nutui-react-taro";
import { Star, ArrowUp } from "@nutui/icons-react-taro";
import Card from "./WaterfallCard";
type LoadStatus = "loading" | "noMore" | "loaded";
const IndexWaterfall = () => {
  const [loadStatus, setLoadStatus] = useState<LoadStatus>();
  const [playlists, setPlaylists] = useState<any[]>([]);
  const limit = 10;
  const [offset, setOffset] = useState(0);
  const [listLeft, setListLeft] = useState<any[]>([]);
  const [listRight, setListRight] = useState<any[]>([]);
  const queryPaneHeight = (name: string): Promise<number> => {
    return new Promise((resolve) => {
      const query = Taro.createSelectorQuery();
      query
        .select(name)
        .boundingClientRect((res: any) => {
          resolve(res?.height || 0);
        })
        .exec();
    });
  };

  const renderCol = async (item: any) => {
    const [left, right] = await Promise.all([
      queryPaneHeight("#leftColumn"),
      queryPaneHeight("#rightColumn"),
    ]);
    if (left > right) {
      setListRight((pre) => pre.concat(item));
    } else {
      setListLeft((pre) => pre.concat(item));
    }
    setPlaylists((pre) => pre.concat(item));
    setOffset((pre) => pre + 1);
  };
  const getList = async () => {
    if (loadStatus === "loading") {
      return;
    }
    console.log("getList");

    setLoadStatus("loading");
    const res = await getPlayList(limit, offset);
    for (const item of res.playlists) {
      item.playCount = toWan(item.playCount);
      item.opacity = 0;
      await renderCol(item);
    }
    setListRight((pre) => pre.map((item) => ({ ...item, opacity: 1 })));
    setListLeft((pre) => pre.map((item) => ({ ...item, opacity: 1 })));

    setLoadStatus("loaded");
  };

  useEffect(() => {
    getList();
  }, []);

  useReachBottom(() => {
    getList();
  });

  const buildContent = () => {
    return (
      <div className={style.content}>
        <div className={style.column} id="leftColumn">
          {listLeft.map((item) => (
            <Card
              opacity={item.opacity}
              coverImgUrl={item.coverImgUrl}
              name={item.name}
              playCount={item.playCount}
            ></Card>
          ))}
        </div>
        <div className={style.column} id="rightColumn">
          {listRight.map((item) => (
            <Card
              opacity={item.opacity}
              coverImgUrl={item.coverImgUrl}
              name={item.name}
              playCount={item.playCount}
            ></Card>
          ))}
        </div>
      </div>
    );
  };
  const buildLoadMsg = () => {
    if (playlists.length) {
      let msg = "";
      let Icon: React.ReactNode;
      switch (loadStatus) {
        case "loading":
          msg = "加载中";
          Icon = (
            <Loading
              direction="vertical"
              icon={<Star size={16} color="red" />}
            />
          );
          break;
        case "noMore":
          msg = "没有更多了";
          break;
        case "loaded":
          msg = "上拉加载更多";
          Icon = <ArrowUp size={16} color="#aaa" />;
          break;
      }
      return (
        <Divider
          style={{
            color: "#aaa",
            borderColor: "#aaa",
            padding: "0 16px",
            borderStyle: "dashed",
          }}
        >
          <Space>
            {Icon}
            <span className={style.loadMsg}>{msg}</span>
          </Space>
        </Divider>
      );
    }
  };
  const buildEmpty = () => {
    return <Empty description="暂无数据哟" />;
  };
  return (
    <>
      {playlists.length ? buildContent() : buildEmpty()}
      {buildLoadMsg()}
    </>
  );
};
export default IndexWaterfall;
