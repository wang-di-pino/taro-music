import CustomImage from "@/components/CustomImage";
import style from "./index.module.scss";
interface CardProps {
  coverImgUrl: string;
  name: string;
  playCount: number;
  opacity?: number;
}
const WaterfallCard: React.FC<CardProps> = ({
  coverImgUrl,
  name,
  playCount,
  opacity = 0,
}) => {
  // const [opacity, setOpacity] = useState(0);
  // const onLoad = () => {
  //   setOpacity(1);
  // };
  return (
    <div
      className={style.card}
      style={{ backgroundColor: `${opacity ? "none" : "#f3f3f3"}` }}
    >
      <div
        className={style.item}
        style={{ opacity, transition: "opacity 0.2s " }}
      >
        <CustomImage
          className={style.image}
          mode="widthFix"
          src={coverImgUrl}
          // onLoad={onLoad}
        />
        <div className={style.description}>{name}</div>
        <div className={style.payCount}>{playCount}</div>
      </div>
    </div>
  );
};
export default WaterfallCard;
