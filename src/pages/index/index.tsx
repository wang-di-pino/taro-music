import React, { useEffect, useState } from "react";
import { observer, inject } from "mobx-react";
import { getBanner, getSearchPlaceholder } from "@/api";
import CustomSwiper from "@/components/CustomSwiper";
import RootContainer from "@/components/RootContainer";
import { Search } from "@nutui/icons-react-taro";
import Taro, { useShareAppMessage } from "@tarojs/taro";
import CustomNavBar from "@/components/CustomNavBar";
import style from "./index.module.scss";
import IndexWaterfall from "./components/Waterfall";
import { push } from "@/utils/router";
import path from "@/enum/path";
import { StateProps } from "types/state";

interface IndexProps {
  children?: React.ReactNode;
  store: StateProps;
}

const Index: React.FC<IndexProps> = ({ store }) => {
  useShareAppMessage(() => {
    return {
      title: "推荐页分享",
      path: path.INDEX,
    };
  });
  const [banners, setBanners] = useState<any[]>([]);
  const queryBanner = async () => {
    const res = await getBanner();
    setBanners(res.banners);
  };
  const [placeholder, setPlaceholder] = useState("");
  const querySearchPlaceholder = async () => {
    const { data } = await getSearchPlaceholder();
    setPlaceholder(data.showKeyword);
  };
  useEffect(() => {
    Taro.showShareMenu({
      withShareTicket: true,
    });
    queryBanner();
    querySearchPlaceholder();
  }, []);
  const buildSearch = () => {
    return (
      <div
        onClick={() => {
          push(path.SEARCH + "?desc=" + placeholder);
        }}
        className={`${style.search} ${placeholder ? style.searchCheck : ""}`}
      >
        <Search className={style.searchCheck.icon} color="#fff" size={12} />
        {placeholder && (
          <div className={`${style.desc} one-txt-cut`}>{placeholder}</div>
        )}
      </div>
    );
  };
  return (
    <div className={style.index}>
      <RootContainer>
        <CustomNavBar
          className={style.navBar}
          fixed={false}
          leftIcon={buildSearch()}
        />
        {CustomSwiper({ list: banners })}
        <IndexWaterfall />
      </RootContainer>
    </div>
  );
};

export default inject("store")(observer(Index));
