import { Component, PropsWithChildren } from "react";
import { Provider } from "mobx-react";
import counterStore from "./store/counter";
import system from "./store/system";
import "./styles/app.scss";
import "./styles/base.scss";
import Taro from "@tarojs/taro";

const store = {
  counterStore,
  system,
};

class App extends Component<PropsWithChildren> {
  componentDidMount() {
    store.system.getSystemInfo()
    if (Taro.getAccountInfoSync().miniProgram.envVersion !== "develop") {
      Taro.setEnableDebug({
        enableDebug: true,
      });
    }
  }

  componentDidShow() {}

  componentDidHide() {}

  // this.props.children 就是要渲染的页面
  render() {
    return <Provider store={store}>{this.props.children}</Provider>;
  }
}

export default App;
