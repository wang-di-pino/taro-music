import axios from "@/utils/request";
/**
 *
 * @param type 1 首页轮播图 2 分类页轮播图
 * @returns
 */
export const getBanner = (type = 1): any => {
  return axios.get("/banner", { params: { type } });
};
/**
 *
 * @param
 * @returns
 */
export const getSearchPlaceholder = (): any => {
  return axios.get("/search/default");
};
/**
 *
 * @param limit 每页数量
 * @param order hot new
 * @param offset 偏移量
 * @returns
 */
export const getPlayList = (limit = 10, offset = 0): any => {
  return axios.get("/top/playlist", {
    params: { limit, order: "hot", offset },
  });
};
/**
 * 热搜榜详情
 * @returns
 */
export const gethotDetail = (): any => {
  return axios.get("/search/hot/detail");
};
/**
 * 关键字搜索
 * @returns
 */
export const keywordsSearch = ({
  keywords,
  type = "mobile",
}: {
  keywords: string;
  type?: "mobile" | "pc";
}): any => {
 
  return axios.get("/search/suggest", { params: { keywords, type } });
};
