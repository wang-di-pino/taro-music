import { makeAutoObservable } from 'mobx';

class CounterStore {
  counter = 0;

  constructor() {
    makeAutoObservable(this);
  }

  increment() {
    this.counter++;
  }

  decrement() {
    this.counter--;
  }

  incrementAsync() {
    setTimeout(() => {
      this.counter++;
    }, 1000);
  }
}

const counterStore = new CounterStore();
export default counterStore;
