import Taro from "@tarojs/taro";
import { makeAutoObservable } from "mobx";
const SYSTEM_KEY = "SYSTEM_KEY";
const defaultInfo = Taro.getStorageSync(SYSTEM_KEY) as {
  systemInfo: Taro.getSystemInfo.Result;
  menuButtonInfo: Taro.getMenuButtonBoundingClientRect.Rect;
  navBarHeight: number;
  statusBarHeight: number;
};
class System {
  systemInfo = defaultInfo.systemInfo;
  menuButtonInfo = defaultInfo.menuButtonInfo;
  navBarHeight = defaultInfo.navBarHeight ?? 60;
  statusBarHeight = defaultInfo.statusBarHeight ?? 20;
  constructor() {
    makeAutoObservable(this);
  }

  getSystemInfo() {
    if (this.systemInfo) {
      return;
    }
    Taro.getSystemInfo({
      success: (res) => {
        this.systemInfo = res;
        const rect = Taro.getMenuButtonBoundingClientRect();
        this.menuButtonInfo = rect;
        this.statusBarHeight = res.statusBarHeight ?? this.statusBarHeight;
        const height = rect.top - this.statusBarHeight + rect.bottom;
        this.navBarHeight = height;
        Taro.setStorage({
          key: SYSTEM_KEY,
          data: {
            systemInfo: this.systemInfo,
            menuButtonInfo: this.menuButtonInfo,
            navBarHeight: this.navBarHeight,
            statusBarHeight: this.statusBarHeight,
          },
        });
      },
    });
  }
}

const system = new System();
export default system;
