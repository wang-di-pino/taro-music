import { ConfigProvider, Image, Loading } from "@nutui/nutui-react-taro";
import { Star } from "@nutui/icons-react-taro";
import { useEffect, useState } from "react";
import { getCurrentInstance } from "@tarojs/runtime";
import Taro from "@tarojs/taro";
import defaultImg from "@/assets/images/default.png";
interface ImageProps {
  src: string;
  style?: React.CSSProperties;
  mode?: Mode;
  className?: string;
  onLoad?: () => void;
}
type Mode =
  | "scaleToFill"
  | "aspectFit"
  | "aspectFill"
  | "widthFix"
  | "heightFix"
  | "top"
  | "bottom"
  | "center"
  | "left"
  | "right"
  | "top left"
  | "top right"
  | "bottom left"
  | "bottom right";
const CustomImage: React.FC<ImageProps> = ({
  src,
  style,
  mode = "aspectFill",
  className,
  onLoad,
}) => {
  const [url, setUrl] = useState(defaultImg);
  const [onlyName, setOnlyName] = useState("");
  useEffect(() => {
    // 生成一个随机标识
    const randomId = "custom_img__" + Math.random().toString(36).substr(2, 9);
    setOnlyName(randomId);
  }, []);
  useEffect(() => {
    const page = getCurrentInstance().page!;
    const observer = Taro.createIntersectionObserver(page);
    const T = setTimeout(() => {
      observer
        .relativeToViewport({ bottom: 100 })
        .observe("." + onlyName, () => {
          setUrl(src);
        });
    }, 50);
    return () => {
      observer.disconnect();
      clearTimeout(T);
    };
  }, [onlyName]);
  return (
    <>
      <Image
        className={`${className} ${onlyName}`}
        mode={mode}
        src={url}
        onLoad={onLoad}
        style={style}
        loading={
          <ConfigProvider theme={{ nutuiLoadingIconSize: "36px" }}>
            <Loading
              direction="vertical"
              icon={<Star size={32} color="red" />}
            />
          </ConfigProvider>
        }
      ></Image>
    </>
  );
};
export default CustomImage;
