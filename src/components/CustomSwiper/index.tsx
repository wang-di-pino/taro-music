import { useState } from "react";
import { Swiper } from "@nutui/nutui-react-taro";
import style from "./index.module.scss";
import CustomImage from "../CustomImage";

interface CustomSwiperProps {
  list: any[];
}
const CustomSwiper: React.FC<CustomSwiperProps> = ({ list }) => {
  const [current, setCurrent] = useState(0);
  const onChange = (e) => {
    setCurrent(e.detail.current);
  };
  const buildIndicator = () => {
    return (
      <div className={style.indicator}>
        {list.map((item, index) => (
          <div
            className={current === index ? style.itemActive : style.item}
            key={item.bannerId}
          />
        ))}
      </div>
    );
  };
  return (
    <div className={style.CustomSwiper}>
      <Swiper
        loop
        interval={2500}
        onChange={onChange}
        indicator={buildIndicator()}
        autoPlay
      >
        {list.map((item) => (
          <Swiper.Item key={item.bannerId}>
            <CustomImage src={item.pic} />
          </Swiper.Item>
        ))}
      </Swiper>
    </div>
  );
};
export default CustomSwiper;
