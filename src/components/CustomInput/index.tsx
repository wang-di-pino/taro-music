import { Close } from "@nutui/icons-react-taro";
import style from "./index.module.scss";
import { CommonEventFunction, Input, InputProps } from "@tarojs/components";
import { useState } from "react";
// 自定义input组件
interface CustomInputProps {
  value: string;
  onInput: CommonEventFunction<InputProps.inputEventDetail>;
  className?: string;
  type?: string;
  placeholder?: string;
  maxlength?: number;
  disabled?: boolean;
  focus?: boolean;
  confirmType?: keyof InputProps.ConfirmType;
  clickClear?: () => void;
  clearIcon?: React.ReactNode;
  onConfirm?: CommonEventFunction<InputProps.inputValueEventDetail>;
  onFocus?: CommonEventFunction<InputProps.inputForceEventDetail>;
  onBlur?: CommonEventFunction<InputProps.inputValueEventDetail>;
}
const CustomInput: React.FC<CustomInputProps> = (props) => {
  const [focus, setFocus] = useState(props.focus);
  return (
    <div className={`${style.CustomInput} ${props.className}`}>
      <Input
        className={`${style.input} one-txt-cut`}
        type={props.type as any}
        placeholder={props.placeholder}
        maxlength={props.maxlength}
        disabled={props.disabled}
        focus={focus}
        value={props.value}
        onInput={props.onInput}
        confirmType={props.confirmType}
        onBlur={(e) => {
          setTimeout(() => {
            setFocus(false);
          }, 250);
          props.onBlur && props.onBlur(e);
        }}
        onConfirm={props.onConfirm}
        onFocus={(e) => {
          setFocus(true);
          props.onFocus && props.onFocus(e);
        }}
      />
      {props.value && focus && (
        <>
          <div className={style.closeIcon}>
            {props.clearIcon ?? <Close size={16} color="#888" />}
          </div>
          <div onClick={props.clickClear} className={style.tapBlock}></div>
        </>
      )}
    </div>
  );
};
export default CustomInput;
