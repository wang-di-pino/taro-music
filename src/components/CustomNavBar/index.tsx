import style from "./index.module.scss";
import { ArrowLeft } from "@nutui/icons-react-taro";
import { observer, inject } from "mobx-react";
import { StateProps } from "types/state";

interface CustomNavBarProps {
  title?: string | React.ReactNode;
  fixed?: boolean;
  leftIcon?: React.ReactNode;
  className?: string;
  store?: StateProps;
}
const CustomNavBar: React.FC<CustomNavBarProps> = ({
  title = "",
  fixed = true,
  leftIcon = <ArrowLeft size={24} color="#000" />,
  className,
  store,
}) => {
  const Style = {
    paddingTop: store?.system.statusBarHeight,
    height: store?.system.navBarHeight,
  };

  const buildNavBar = () => {
    return (
      <div
        className={`${style.CustomNavBar} ${
          fixed ? style.fixed : ""
        } ${className}`}
        style={Style}
      >
        <div className={style.leftIcon}>{leftIcon}</div>
        <div className={style.title}>{title}</div>
        <div className={style.rightIcon}></div>
      </div>
    );
  };
  return buildNavBar();
};
export default inject("store")(observer(CustomNavBar));
