import React, { ReactNode } from "react";
import style from "./index.module.scss";

interface ContainerProps {
  children: ReactNode;
}
const RootContainer: React.FC<ContainerProps> = ({ children }) => {
  return (
    <>
      <div className={style.RootContainer}>
        {children}
      </div>
    </>
  );
};
export default RootContainer;
