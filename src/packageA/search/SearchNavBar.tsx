import style from "./index.module.scss";
import { ArrowLeft } from "@nutui/icons-react-taro";
import React, { useEffect, useState } from "react";
import { useRouter } from "@tarojs/taro";
import CustomInput from "@/components/CustomInput";
import { back } from "@/utils/router";
import CustomNavBar from "@/components/CustomNavBar";
import { keywordsSearch } from "@/api";
import { Search } from "@nutui/icons-react-taro";

const SearchNavBar: React.FC = () => {
  const [value, setValue] = useState("");
  const [allMatch, setAllMatch] = useState<any[]>([]);
  const router = useRouter();
  const placeholder = decodeURI(router.params.desc!);
  const queryKeywords = async () => {
    if (!value) {
      return;
    }
    const {
      result: { allMatch },
    } = await keywordsSearch({ keywords: value });
    if (allMatch) {
      setAllMatch(allMatch);
    }
  };
  const buildPopover = () => {
    const handleClick = (item) => {
      setValue(item.keyword);
      setTimeout(() => {
        setAllMatch([]);
      }, 100);
    };
    const buildItem = (item) => {
      return (
        <div className={style.popoverItem} onClick={() => handleClick(item)}>
          <Search color="#ffa652" size={16} />
          {item.keyword}
        </div>
      );
    };
    return allMatch.length && value ? (
      <div className={style.popover}>
        <div className={style.popoverTitle}>搜索“{value}”</div>
        {allMatch.map((item) => buildItem(item))}
      </div>
    ) : null;
  };
  useEffect(() => {
    queryKeywords();
    value || setAllMatch([]);
  }, [value]);
  return (
    <>
      <CustomNavBar
        fixed={false}
        leftIcon={
          <div className={style.navBarTitle}>
            <ArrowLeft size={24} color="#000" onClick={() => back()} />
            <div className={style.searchInput}>
              <CustomInput
                value={value}
                onInput={(e) => setValue(e.detail.value.trim())}
                clickClear={() => setValue("")}
                placeholder={placeholder}
                focus={true}
                onFocus={() => {
                  console.log("onFocus");
                }}
                onBlur={() => {
                  console.log("onBlur");
                }}
                onConfirm={() => {
                  console.log("onConfirm");
                }}
                confirmType="search"
              />
              {buildPopover()}
            </div>
          </div>
        }
      ></CustomNavBar>
      {buildPopover}
    </>
  );
};
export default SearchNavBar;
