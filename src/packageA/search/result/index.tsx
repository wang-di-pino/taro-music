import RootContainer from "@/components/RootContainer";
import style from "./index.module.scss";
import SearchNavBar from "../SearchNavBar";
const SearchResult: React.FC = () => {
  const buildContent = () => {
    return (
      <div className={style.content}>
        <div className={style.txtCut}>
          嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻
          嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻
          嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻
          嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻嘻 嘻嘻嘻嘻嘻嘻嘻
          <span className={style.label} />
        </div>
      </div>
    );
  };
  return (
    <RootContainer>
      <SearchNavBar />
      {buildContent()}
    </RootContainer>
  );
};
export default SearchResult;
