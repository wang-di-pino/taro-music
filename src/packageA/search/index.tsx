import RootContainer from "@/components/RootContainer";
import style from "./index.module.scss";
import SearchNavBar from "./SearchNavBar";
import { useEffect, useState } from "react";
import { gethotDetail } from "@/api";
import CustomImage from "@/components/CustomImage";
import { push } from "@/utils/router";
import path from "@/enum/path";
const Search: React.FC = () => {
  const buildContent = () => {
    const [hotDetail, setHotDetail] = useState<any[]>([]);
    const gethotDetailList = async () => {
      const { data } = await gethotDetail();
      setHotDetail(data);
    };
    useEffect(() => {
      gethotDetailList();
    }, []);
    const buildItem = (item, index) => {
      return (
        <div onClick={() => push(path.SEARCH_RESULT+"?desc=" + item.searchWord)} className={`${style.item} ${index < 3 ? style.itemActive : ""}`}>
          <span className={style.index}>{index + 1}</span>
          <span className={`${style.searchWord} one-txt-cut`}>
            {item.searchWord}
          </span>
          {item.iconUrl && (
            <CustomImage mode="widthFix" src={item.iconUrl} className={style.icon} />
          )}
        </div>
      );
    };
    return (
      <>
        <div className={style.title}>热搜榜单</div>
        <div className={style.content}>
          {hotDetail.map((item, index) => {
            return buildItem(item, index);
          })}
        </div>
      </>
    );
  };
  return (
    <RootContainer>
      <SearchNavBar />
      {buildContent()}
    </RootContainer>
  );
};
export default Search;
