/* 封装axios */
import axios, { AxiosResponse, InternalAxiosRequestConfig } from "axios";

const baseURL = process.env.TARO_APP_BASE_URL;
// 使用axios来创建axios的实例对象并配置
const instance = axios.create({
  baseURL,
  // 设置超时时间
  timeout: 30000,
});
export const requestSuccess = async function (
  config: InternalAxiosRequestConfig
) {
  // config.headers.token = "";

  return config;
};

export const responseSuccess = (response: AxiosResponse) => {
  const res = response.data;
  return res;
};

export const responseFail = (error: any) => {
  console.dir(error, "error");
  return Promise.reject(error);
};

// 添加请求拦截器
instance.interceptors.request.use(requestSuccess, null);

// 添加响应拦截器
instance.interceptors.response.use(responseSuccess, responseFail);

// 对外导出axios的实例对象使用
export default instance;
