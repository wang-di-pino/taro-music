import Taro from "@tarojs/taro";

/**
 * 路由navigateTo
 * @param url
 */
export const push = (url: string) => {
  Taro.navigateTo({
    url,
  });
};
/**
 * 路由回退
 * @param delta
 */
export const back = (delta = 1) => {
  //回退
  Taro.navigateBack({
    delta,
  });
};
