/**
 * 转换为万
 * @param num
 * @param zh
 */ export function toWan(num: number, zh = true) {
  return (num / 10000).toFixed(1) + (zh ? "万" : "w");
}
