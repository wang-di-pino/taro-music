module.exports = {
  extends: ["taro/react"],
  rules: {
    "react/jsx-uses-react": "off",
    "react/react-in-jsx-scope": "off",
    "no-multiple-empty-lines": ["error", { max: 1 }],
    "import/first": ["error", "off"],
  },
};
