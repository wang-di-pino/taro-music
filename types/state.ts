export interface StateProps {
  system: {
    systemInfo: Taro.getSystemInfo.Result;
    menuButtonInfo: Taro.getMenuButtonBoundingClientRect.Rect;
    navBarHeight: number;
    statusBarHeight: number;
  };
}
